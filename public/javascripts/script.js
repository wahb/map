$(document).ready( 
    function() {
        $("#driverData").hide();
        $.ajax({          
            type:'GET',
            url:'http://localhost:3000/cars',
            dataType:'json',              
            success: function(data){
                $.each(data, function (i, item) {
                   $('#drivers').append($('<option>', { 
                       value: item.driverName,
                       text : item.driverName
                   }));
               });
            }
         });
        $('#drivers').on('change', function(){
             var driver = $('#drivers').val()
             $.ajax({          
                 type:'GET',
                 url:'http://localhost:3000/cars/?driverName='+driver,
                 dataType:'json',              
                 success: function(data){
                     $('#driverData').show();
                     data = data[0];
                     $('#driverName').html('<b>Name: </b>'+data.driverName);
                     $('#driverCityOrigin').html('<b>City: </b>'+data.driverCityOrigin);
                     $('#driverLanguage').html('<b>Language: </b>'+data.driverLanguage);
                     $('#driverPhone').html('<b>Phone: </b>'+data.driverPhone);
                     $('#driverGender').html('<b>Gender: </b>'+data.driverGender);
                     $('#driverInfo').html('<b>Info: </b>'+data.driverInfo);
                     $('#carMake').html('<b>Car: </b>'+data.carMake);
                     $('#kmDriven').html('<b>Driven KM: </b>'+data.kmDriven);
                }
             });
             setInterval(function(){
                 $.ajax({          
                 type:'GET',
                 url:'http://localhost:3000/cars/?driverName='+driver,
                 dataType:'json',              
                 success: function(data){
                     var position = new google.maps.LatLng(eval(data[0].location[0]), eval(data[0].location[1])) 
                     map.setCenter(position);
                     marker.setPosition(position)
                }
             });
        }, 5000)
      });
    }
);
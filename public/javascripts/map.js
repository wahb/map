var position = [48.1545703,11.2616557, 10];
var directionsDisplay;
var map;
var marker = new google.maps.Marker();

var service = new google.maps.DistanceMatrixService;

function initialize() {

    directionsDisplay = new google.maps.DirectionsRenderer();

    var myOptions = {
       zoom: 15,
       streetViewControl: false,
       scaleControl: false,
       mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById('googlemaps'), myOptions);

    latLng = new google.maps.LatLng(position[0], position[1]);

    map.setCenter(latLng);
    directionsDisplay.setMap(map);

    marker.setPosition(latLng)
    marker.setMap(map)
    marker.setIcon('/images/car.png')
}

google.maps.event.addDomListener(window, 'load', initialize);
// import modules

var express = require('express');
var path = require('path');
var fs = require('fs');
var randopeep = require('randopeep');
var jsonServer = require('json-server');

fs.writeFileSync("./cars/index.get.json", '');

const low = require('lowdb')
const storage = require('lowdb/file-sync')
const db = low('./cars/index.get.json', { storage })

// init express
var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(express.static(path.join(__dirname, 'public')));
app.use('/jquery', express.static(path.join(__dirname, '/node_modules/jquery/dist/')));

app.use('/', routes);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;

//Generate 10 objects to work with in the backend for the front end

fakeit(10)

function fakeit(k) {
    //var o = JSON.parse(fs.readFileSync('./cars/index.get.json', 'utf8'));
    //data = { cars : [] }
    for (var i=0; i < k; i++){
        db('cars').push({
            driverName: randopeep.name(),
            driverCityOrigin: randopeep.address.city(),
            driverLanguage: ['de', 'en', 'nl', 'fr', 'es', 'ar'][Math.floor(Math.random()*7)],
            driverPhone: randopeep.address.phone(),
            driverGender: ['male', 'female'][Math.floor(Math.random()*2)],
            driverInfo: randopeep.corporate.catchPhrase(0),
            carMake: randopeep.corporate.name('large', 0),
            kmDriven: Math.floor(Math.random() * 100000),
            location: randopeep.address.geo()
       });
    }
    // db('cars').save
    //o.push(data);
    // fs.writeFileSync("./cars/index.get.json", JSON.stringify(data));
}

// Create the REST API server
var server = jsonServer.create()
var router = jsonServer.router('./cars/index.get.json')
server.use(jsonServer.defaults())
server.use(router)
server.listen(3000)

setInterval(function() {
    // var o = JSON.parse(fs.readFileSync('./cars/index.get.json', 'utf8'));

    for (var i = 0; i < db('cars').size(); i++) {
        db('cars').at(i)[0].location = randopeep.address.geo();
        router.db('cars').at(i)[0].location = randopeep.address.geo();
    }
    // fs.writeFileSync("./cars/index.get.json", JSON.stringify(o));
    db.read()
    router.db.save()
}, 5000);

// start express
app.listen(8080, function () {
console.log("express has started on port 8080");
});
